// src/model/user.js

const {SchameTypes, Schema} = require('mongoose');

module.exports = class extends think.Mongoose {
  get schema() {
    var _schema = new Schema({
      uid: { // 转发人的 userId
        type: String
      },
      aid: { // 转发的文章ID
        type: String,
      },
      created_time: { // 创建时间默认当前时间
        type: Date, default: Date.now
      }
    })

    // // 定义get方法 查询是返回
    // _schema.virtual('isAdvanced').get(function () {
    //   // 积分高于 700 则认为是高级用户
    //   return this.score > 700 || this.is_star;
    // });

    _schema.index({uid: 1, aid: 1});

    _schema.pre('save',  function(next){
      // 保存进数据库之前的一些操作
      next();
    });
    return _schema;
  }
}