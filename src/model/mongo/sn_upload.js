// src/model/user.js

const {SchameTypes, Schema} = require('mongoose');

module.exports = class extends think.Mongoose {
  get schema() {
    var _schema = new Schema({
      uid: { // 上传者的 userId
        type: String, default: ''
      },
      path: { // 资源的本地路径
        type: String, default: ''
      },
      url: { // 资源连接
        type: String, default: ''
      },
      exturl: { // 外部资源连接， cdn 或者 oss的
        type: String, default: ''
      },
      visits: { // 打开次数
        type: Number, default: 0
      },
      upload_time: { // 上传时间
        type: Date, default: Date.now
      }
    })
    return _schema;
  }
}