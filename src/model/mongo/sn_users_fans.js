// src/model/user.js

const {SchameTypes, Schema} = require('mongoose');

module.exports = class extends think.Mongoose {
  get schema() {
    var _schema = new Schema({
      uid: { // 用户ID
        type: String
      },
      to_id: { // 目标用户ID
        type: String
      },
      created_time: { // 创建时间默认当前时间
        type: Date, default: Date.now
      },
      update_time: { // 更新时间
        type: Date, default: Date.now
      },
      status: { // 当前状态 取消关注为0
        type: Number, default: 1
      },
    })

    // // 定义get方法 查询是返回
    // _schema.virtual('isAdvanced').get(function () {
    //   // 积分高于 700 则认为是高级用户
    //   return this.score > 700 || this.is_star;
    // });

    _schema.index({uid: 1, to_id: 1,status: 1, update_time: -1});

    _schema.pre('save',  function(next){
      // 保存进数据库之前的一些操作
      this.update_time = new Date();
      next();
    });
    return _schema;
  }
}