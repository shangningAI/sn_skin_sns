// src/model/user.js

const {SchameTypes, Schema} = require('mongoose');

module.exports = class extends think.Mongoose {
  get schema() {
    var _schema = new Schema({
      uid: { // 用户的id
        type: String, default: ''
      },
      nickname: { // 昵称
        type: String, default: ''
      },
      sex: { // 性别 默认未知
        type: Number, default: -1
      },
      headpic: { // 头像
        type: String, default: ''
      },
      tagid_list: { // 用户喜欢的类型标签
        type: Array, default: []
      },
      country: { // 国家
        type: String, default: ''
      },
      province: { // 省份
        type: String, default: ''
      },
      city: { // 城市
        type: String, default: ''
      },
      region: { // 区域
        type: String, default: ''
      },
      remark: { // 备注
        type: String, default: ''
      },
      created_time: { // 创建时间默认当前时间
        type: Date, default: Date.now
      }
    })

    // // 定义get方法 查询是返回
    // _schema.virtual('isAdvanced').get(function () {
    //   // 积分高于 700 则认为是高级用户
    //   return this.score > 700 || this.is_star;
    // });

    _schema.index({uid: 1}, {unique: true});
    _schema.index({nickname: 1});

    _schema.pre('save',  function(next){
      // 保存进数据库之前的一些操作
      next();
    });
    return _schema;
  }
}