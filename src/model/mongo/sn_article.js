// src/model/user.js

const {SchameTypes, Schema} = require('mongoose');

module.exports = class extends think.Mongoose {
  get schema() {
    var _schema = new Schema({
      uid: { // 发布者的 userId
        type: String
      },
      title: { // 微信用户的unionid
        type: String, default: ''
      },
      content: { // 文章的文字内容
        type: String, default: ''
      },
      type: { // 文章类型 默认为 1:文字  2:视频  3:音频  4:图片 5:图文
        type: Number,default: 1
      },
      video: { // 视频信息
        cover: String, // 封面图
        duration: Number,// 时长秒
        url: String, // 视频
        width: Number, // 视频宽度
        height: Number, //  视频高度
      },
      images:[{
        url: String,
        width: Number, // 视频宽度
        height: Number, //  视频高度
      }],
      audio:{
        url: String, // 视频
        duration: Number,// 时长秒
      },
      imgtext: { // 图文消息的富文本内容
        type: String,
      },
      tagid_listtagid_list: { // 文章的标签id
        type: Array, default: []
      },
      status: { // 文章状态 0 为 已删除 1为已审核 2为审核中
        type: Number,default: 1
      },
      give: { // 文章点赞数
        type: Number,default: 0
      },
      comment: { // 文章评论
        type: Number,default: 0
      },
      share: { // 文章分享数
        type: Number,default: 0
      },
      remark: { // 文章备注
        type: String,default: ''
      },
      created_time: { // 创建时间默认当前时间
        type: Date, default: Date.now
      }
    })

    // _schema.index({type: 1, created_time: -1});
    // _schema.index({title: 1});

    // _schema.pre('save',  function(next){
    //   // 保存进数据库之前的一些操作
    //   next();
    // });
    return _schema;
  }
}