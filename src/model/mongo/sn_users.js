// src/model/user.js

const {SchameTypes, Schema} = require('mongoose');

module.exports = class extends think.Mongoose {
  get schema() {
    var _schema = new Schema({
      phone: { // 用户手机号码
        type: String
      },
      unionid: { // 微信用户的unionid
        type: String
      },
      openid: { // 微信用户的openid
        type: String
      },
      password: { // 用户密码
        type: String
      },
      status: {// 用户状态 封禁为0
        type: Number, default: 1
      }
    })

    // // 定义get方法 查询是返回
    // _schema.virtual('isAdvanced').get(function () {
    //   // 积分高于 700 则认为是高级用户
    //   return this.score > 700 || this.is_star;
    // });

    _schema.index({phone: 1});
    _schema.index({unionid: 1});
    _schema.index({openid: 1});

    _schema.pre('save',  function(next){
      // 保存进数据库之前的一些操作
      next();
    });
    return _schema;
  }
}