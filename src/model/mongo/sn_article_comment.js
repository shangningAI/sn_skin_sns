// src/model/user.js

const {SchameTypes, Schema} = require('mongoose');

module.exports = class extends think.Mongoose {
  get schema() {
    var _schema = new Schema({
      uid: { // 评论人的 userId
        type: String
      },
      aid: { // 评论的文章ID
        type: String,
      },
      cid: { // 评论的上级ID 默认为空 为一楼没有上级
        type: String, default: ''
      },
      content: { // 评论的文字内容
        type: String
      },
      give: { // 评论点赞数
        type: Number,default: 0
      },
      comment: { // 下级评论条数
        type: Number,  default: 0
      },
      type: { // 评论的资源类型 默认为1 纯文字 视频 语音 图片
        type: Number,  default: 1
      },
      video: { // 视频信息
        cover: String, // 封面图
        duration: Number,// 时长秒
        url: String, // 视频
        width: Number, // 视频宽度
        height: Number, //  视频高度
      },
      images:[{
        url: String,
        width: Number, // 视频宽度
        height: Number, //  视频高度
      }],
      audio:{
        url: String, // 视频
        duration: Number,// 时长秒
      },
      status: { // 评论状态 1为正常  0为删除
        type: Number, default: 1
      },
      created_time: { // 评论时间
        type: Date, default: Date.now
      }
    })

    // // 定义get方法 查询是返回
    // _schema.virtual('isAdvanced').get(function () {
    //   // 积分高于 700 则认为是高级用户
    //   return this.score > 700 || this.is_star;
    // });

    _schema.index({uid: 1, aid: 1, cid: 1, created_time: -1});

    _schema.pre('save',  function(next){
      // 保存进数据库之前的一些操作
      next();
    });
    return _schema;
  }
}