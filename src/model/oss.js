let OSS = require('ali-oss');
module.exports = class extends think.Model {
  useOSS (level) {
    var level = level || 'snskin1'
    let client = new OSS(think.config('oss'))
    client.useBucket(level);
    return client
  }
};



