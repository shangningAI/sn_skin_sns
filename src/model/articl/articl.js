var mongoose = require('mongoose');

module.exports = class extends think.Model {
  /**
   * 获取文章列表
   * @param {Object} data 查询参数可为空
   */
  async getList (data) {
    console.log(data)
    data = data || {}
    const db = think.mongoose('mongo/sn_article');
    var _id = data.aid ? mongoose.Types.ObjectId(data.aid):null
    var _data = {
      type: data.type ? parseInt(data.type) : {$ne: 0},
      _id: _id ? {$lt: _id} : {$ne: mongoose.Types.ObjectId(0)}
    }
    console.log(_data)
    var result = await db.find(_data).sort({_id: -1}).limit(parseInt(data.num) || 50)
    return result;
  }
};
