module.exports = class extends think.Logic {
  indexAction() {
    this.allowMethods = 'post'; //  只允许 POST 请求类型
    let rules = {
      type: {
        int: true,       // 字段类型为 String 类型
        required: true,     // 字段必填
        method: 'POST'       // 指定获取数据的方式
      }
    }
    let flag = this.validate(rules);
    if (!flag) {
      return this.fail('validate error', this.validateErrors);
    }
  }
};
