const fileCache = require('think-cache-file');
const nunjucks = require('think-view-nunjucks');
const {Console, File, DateFile} = require('think-logger3');
const path = require('path');
const isDev = think.env === 'development';
const mongoose = require('think-mongoose');
const JWTSession = require('think-session-jwt');

/**
 * cache adapter config
 * @type {Object}
 */
exports.cache = {
  type: 'file',
  common: {
    timeout: 3153600000000
  },
  file: {
    handle: fileCache,
    cachePath: path.join(think.ROOT_PATH, 'runtime/cache'), // absoulte path is necessarily required
    pathDepth: 1,
    gcInterval: 3153600000000 
  }
};

/**
 * model adapter config
 * @type {Object}
 */
exports.model = {
  type: 'mongoose',
  mongoose: {
    handle: mongoose,
    host: 'localhost',
    user: 'bbstest',
    password: '123456',
    database: 'bbstest',
    useCollectionPlural: false,
    options: {}
  }
}

/**
 * session adapter config
 * @type {Object}
 */
exports.session = {
  type: 'jwt',
  common: {
    cookie: {
      name: 'thinkjs',
    }
  },
  jwt: {
    handle: JWTSession,
    secret: 'hgzxcsa472135', // secret is reqired // 密钥
    tokenType: 'header', // ['query', 'body', 'header', 'cookie'], 'cookie' is default // token来源 header
    tokenName: 'jwt', // if tokenType not 'cookie', this will be token name, 'jwt' is default // token 名字
    sign: {
      // sign options is not required
    },
    verify: {
      // verify options is not required
    },
    verifyCallback: any => any, // default verify fail callback
  }
}

/**
 * view adapter config
 * @type {Object}
 */
exports.view = {
  type: 'nunjucks',
  common: {
    viewPath: path.join(think.ROOT_PATH, 'view'),
    sep: '_',
    extname: '.html'
  },
  nunjucks: {
    handle: nunjucks
  }
};

/**
 * logger adapter config
 * @type {Object}
 */
exports.logger = {
  type: isDev ? 'console' : 'dateFile',
  console: {
    handle: Console
  },
  file: {
    handle: File,
    backups: 10, // max chunk number
    absolute: true,
    maxLogSize: 50 * 1024, // 50M
    filename: path.join(think.ROOT_PATH, 'logs/app.log')
  },
  dateFile: {
    handle: DateFile,
    level: 'ALL',
    absolute: true,
    pattern: '-yyyy-MM-dd',
    alwaysIncludePattern: true,
    filename: path.join(think.ROOT_PATH, 'logs/app.log')
  }
};
