const crypto = require('crypto');
var mongoose = require('mongoose');

module.exports = {
  // 基于sha256  的加密算法
  sha256 (str) {
    let hmac = crypto.createHmac('sha256', think.config('_keys'));
    hmac.update(str);
    return hmac.digest('hex');
  },
  objId (_id) {
    return mongoose.Types.ObjectId(_id)
  }
}