const Base = require('../base.js');

module.exports = class extends Base {
  async indexAction() {
    try {
      var userdb = this.mongoose('mongo/sn_users')
      var findUser = await userdb.find({_id: think.objId(this.get('uid'))})
      if (findUser.length > 0) {
        if (findUser[0].status != 1) {
          return this.json({
            status: 0,
            msg: '用户已被封禁'
          })
        }
        var userInfodb = this.mongoose('mongo/sn_users_info')
        var findInfo = await userInfodb.find({uid: this.get('uid')})
        return this.json({
          status: 1,
          info: findInfo[0]
        })
      } else {
        return this.json({
          status: -1,
          msg: '用户不存在'
        })
      }
    } catch (e) {
      return this.json({
        status: -2,
        msg: e.message
      })
    }
  }
};
