const NeedLogin = require('../need_login.js');

module.exports = class extends NeedLogin {
  async indexAction() {
    try {
      var data = this.post();
      var infoDb = this.mongoose('mongo/sn_users_info')
      var findUserInfo = await infoDb.find({uid: data.uid})
      if (findUserInfo.length > 0) {
        await infoDb.update({uid: data.uid}, think.omit(data, 'uid'))
        return this.json({
          status: 1,
          msg: '修改成功'  
        })
      } else {
        return this.json({
          status: -1,
          msg: '当前用户信息不存在'  
        })
      }
    } catch (e) {
      return this.json({
        status: -2,
        msg: e.message  
      })
    }
  }
};
