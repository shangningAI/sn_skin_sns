const Base = require('../base.js');

module.exports = class extends Base {
  async indexAction() {
    try {
      var articledb = this.mongoose('mongo/sn_article')
      var findArticle = await articledb.find({_id: think.objId(this.get('id'))})
      if (findArticle.length > 0) {
        if (findArticle[0].status == 1) {
          return this.json({
            status: 1,
            details: findArticle[0]
          })
        } else {
          return this.json({
            status: 0,
            msg: '文章违规已被删除'
          })
        }
      } else {
        return this.json({
          status: -1,
          msg: '文章不存在'
        })
      }
    } catch (e) {
      return this.json({
        status: -2,
        msg: e.message
      })
    }
  }
};
