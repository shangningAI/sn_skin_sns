const Base = require('../base.js');

module.exports = class extends Base {
  async indexAction() {
    var ajaxData;
    if(this.isGet) ajaxData = this.get()
    if(this.isPost) ajaxData = this.post()
    console.log('接收参数' + JSON.stringify(ajaxData));
    var result = await this.model('articl/articl').getList(ajaxData)
    this.json(result)
  }
};