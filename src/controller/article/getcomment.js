const Base = require('../base.js');

module.exports = class extends Base {
  async indexAction() {
    var ajaxData;
    if(this.isGet) ajaxData = this.get()
    if(this.isPost) ajaxData = this.post()
    try {
      var commentDb = this.mongoose('mongo/sn_article_comment')
      var result = await commentDb.find({
        aid: ajaxData.aid,
        cid: ajaxData.cid || '',
        status: 1
      }, {
        _id: 1,
        cid: 1,
        uid: 1,
        aid: 1,
        give: 1,
        comment: 1,
        content: 1,
        images: 1,
        video: 1,
        audio: 1,
        created_time: 1
      });
      return this.json({
        status: 1,
        result
      })
    } catch (e) {
      return this.json({
        status: -2,
        msg: e.message
      })
    }
  }
};
