const NeedLogin = require('../need_login.js');

module.exports = class extends NeedLogin {
  async indexAction() {
    try {
      var db = this.mongoose('mongo/sn_article')
      var data = new db(this.post())
      var article = await data.save()
      const value = think.omit(article, 'title,__v');
      this.json(value)
    } catch (e) {
      return this.json({
        status: -2,
        msg: e.message
      })
    }
  }
};
