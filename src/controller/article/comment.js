const NeedLogin = require('../need_login.js');

module.exports = class extends NeedLogin {
  async indexAction() {
    var ajaxData = this.post()
    console.log('post参数', ajaxData)
    // 查询 数据库
    try {
      var articleDb = this.mongoose('mongo/sn_article')
      var _aid = think.objId(ajaxData.aid)
      console.log('文章ID', _aid)
      var findArticle = await articleDb.find({
        _id: _aid
      });
      console.log('查询结果', findArticle)
      if (findArticle.length <= 0) {
        return this.json({
          status: -1,
          msg: '文章ID不存在'
        })
      }
      var commentDb = this.mongoose('mongo/sn_article_comment')
      // 保存评论
      var _data = {
        uid: ajaxData.uid,
        aid: ajaxData.aid,
        cid: ajaxData.cid || '',
        content: ajaxData.content,
        type: ajaxData.type || 1,
      }
      if (_data.type == 2) { // 视频
        _data.video = ajaxData.video
      } else if (_data.type == 3) { // 语音
        _data.images = ajaxData.images
      } else if (_data.type == 4) { // 图片
        _data.audio = ajaxData.audio
      }
      var data = new commentDb(_data);
      var saveData = await data.save();
      var article_count = await commentDb.find({
        uid: ajaxData.uid,
        aid: ajaxData.aid,
        status: 1
      }).count();
      var comment_count;
      if (ajaxData.cid) {
        comment_count = await commentDb.find({
          uid: ajaxData.uid,
          aid: ajaxData.aid,
          cid: ajaxData.cid,
          status: 1
        }).count();
      }
      return this.json({
        status: 1,
        data: saveData,
        article_count: article_count,
        comment_count: comment_count
      })
    } catch (e) {
      return this.json({
        status: -2,
        msg: e.message
      })
    }
  }
  async giveAction() { // 评论点赞
    var ajaxData;
    if(this.isGet) ajaxData = this.get()
    if(this.isPost) ajaxData = this.post()
    try {
      var commentGiveDb = this.mongoose('mongo/sn_comment_give')
      var result = await commentGiveDb.find({
        uid: ajaxData.uid,
        cid: ajaxData.cid
      })
      var current;
      if (result.length > 0) {
        await commentGiveDb.update({
          uid: ajaxData.uid,
          cid: ajaxData.cid
        }, {
          status: result[0].status == 1 ? 0 : 1
        })
        current = result[0].status == 1 ? 0 : 1
      } else {
        var data = new commentGiveDb({
          uid: ajaxData.uid,
          cid: ajaxData.cid
        })
        await data.save();
        current = 1
      }
      var findGiveCount = await commentGiveDb.find({
        uid: ajaxData.uid,
        cid: ajaxData.cid,
        status: 1
      }).count()
      var commentDb = this.mongoose('mongo/sn_article_comment')
      await commentDb.update({
        _id: think.objId(ajaxData.cid),
      }, { give: findGiveCount });
      return this.json({
        status: 1,
        current: current,
        count: findGiveCount
      })
    } catch (e) {
      return this.json({
        status: -2,
        msg: e.message
      })
    }
  }
};
