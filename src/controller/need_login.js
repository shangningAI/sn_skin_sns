module.exports = class extends think.Controller {
  async __before() {
    console.log('初始POST参数', this.post())
    var user =  await this.session('login')
    if (!user) {
      return this.fail(1002, 'need login');
    }
    if(this.isGet) this.get('uid', user)
    if(this.isPost) this.post('uid', user)
  }
};
